
//Each thread load exactly one halo pixel
//Thus, we assume that the halo size is not larger than the 
//dimension of the work-group in the direction of the kernel

//to efficiently reduce the memory transfer overhead of the global memory
// (each pixel is lodaded multiple times at high overlaps)
// one work-item will compute RESULT_STEPS pixels

//for unrolling loops, these values have to be known at compile time

/* These macros will be defined dynamically during building the program

#define KERNEL_RADIUS 2

//horizontal kernel
#define H_GROUPSIZE_X		32
#define H_GROUPSIZE_Y		4
#define H_RESULT_STEPS		2

//vertical kernel
#define V_GROUPSIZE_X		32
#define V_GROUPSIZE_Y		16
#define V_RESULT_STEPS		3

*/

#define KERNEL_LENGTH (2 * KERNEL_RADIUS + 1)

// macros for easier image access
#define INDEX(x, y) ((y) * Pitch + (x))
#define CHECK_BOUNDS(i, s) ((i) >= 0 && (i) < (s))
#define H_DATA(x, y) (CHECK_BOUNDS(x, Width) ? d_Src[INDEX(x, y)] : 0)
#define V_DATA(x, y) (CHECK_BOUNDS(y, Height) ? d_Src[INDEX(x, y)] : 0)


//////////////////////////////////////////////////////////////////////////////////////////////////////
// Horizontal convolution filter

/*
c_Kernel stores 2 * KERNEL_RADIUS + 1 weights, use these during the convolution
*/

//require matching work-group size
__kernel __attribute__((reqd_work_group_size(H_GROUPSIZE_X, H_GROUPSIZE_Y, 1)))
void ConvHorizontal(
			__global float* d_Dst,
			__global const float* d_Src,
			__constant float* c_Kernel,
			int Width,
			int Pitch
			)
{
	//The size of the local memory: one value for each work-item.
	//We even load unused pixels to the halo area, to keep the code and local memory access simple.
	//Since these loads are coalesced, they introduce no overhead, except for slightly redundant local memory allocation.
	//Each work-item loads H_RESULT_STEPS values + 2 halo values
	__local float tile[H_GROUPSIZE_Y][(H_RESULT_STEPS + 2) * H_GROUPSIZE_X];

	int2 lid;
	lid.x = get_local_id(0);
	lid.y = get_local_id(1);

	int2 lb;
	lb.x = get_group_id(0);
	lb.y = get_group_id(1);

	const int baseX = lb.x * H_RESULT_STEPS * H_GROUPSIZE_X - H_GROUPSIZE_X;
	const int baseY = lb.y * H_GROUPSIZE_Y;

	// Load left halo (check for left bound)
	// Load main data + right halo (check for right bound)
#pragma unroll
	for (int tileID = 0; tileID < H_RESULT_STEPS + 2; tileID++)
	{
		tile[lid.y][H_GROUPSIZE_X * tileID + lid.x] = H_DATA(baseX + H_GROUPSIZE_X * tileID + lid.x, baseY + lid.y);
	}

	// Sync the work-items after loading
	barrier(CLK_LOCAL_MEM_FENCE);

	// Convolve and store the result
#pragma unroll
	for (int tileID = 1; tileID <= H_RESULT_STEPS; tileID++)
	{
		int lx = H_GROUPSIZE_X * tileID + lid.x;
		if (!CHECK_BOUNDS(baseX + lx, Width))
			continue;

		float result = 0;
#pragma unroll
		for (int i = -KERNEL_RADIUS; i <= KERNEL_RADIUS; i++)
		{
			result += tile[lid.y][lx + i] * c_Kernel[KERNEL_RADIUS + i];
		}

		d_Dst[INDEX(baseX + lx, baseY + lid.y)] = result;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Vertical convolution filter

//require matching work-group size
__kernel __attribute__((reqd_work_group_size(V_GROUPSIZE_X, V_GROUPSIZE_Y, 1)))
void ConvVertical(
			__global float* d_Dst,
			__global const float* d_Src,
			__constant float* c_Kernel,
			int Height,
			int Pitch
			)
{
	__local float tile[(V_RESULT_STEPS + 2) * V_GROUPSIZE_Y][V_GROUPSIZE_X];

	int2 lid;
	lid.x = get_local_id(0);
	lid.y = get_local_id(1);

	int2 lb;
	lb.x = get_group_id(0);
	lb.y = get_group_id(1);

	const int baseX = lb.x * V_GROUPSIZE_X;
	const int baseY = lb.y * V_RESULT_STEPS * V_GROUPSIZE_Y - V_GROUPSIZE_Y;

	// Conceptually similar to ConvHorizontal
	// Load top halo + main data + bottom halo
#pragma unroll
	for (int tileID = 0; tileID < V_RESULT_STEPS + 2; tileID++)
	{
		tile[V_GROUPSIZE_Y * tileID + lid.y][lid.x] = V_DATA(baseX + lid.x, baseY + V_GROUPSIZE_Y * tileID + lid.y);
	}

	// Sync the work-items after loading
	barrier(CLK_LOCAL_MEM_FENCE);

	// Compute and store results
#pragma unroll
	for (int tileID = 1; tileID <= V_RESULT_STEPS; tileID++)
	{
		int ly = V_GROUPSIZE_Y * tileID + lid.y;
		if (!CHECK_BOUNDS(baseY + ly, Height))
			continue;

		float result = 0;
#pragma unroll
		for (int i = -KERNEL_RADIUS; i <= KERNEL_RADIUS; i++)
		{
			result += tile[ly + i][lid.x] * c_Kernel[KERNEL_RADIUS + i];
		}

		d_Dst[INDEX(baseX + lid.x, baseY + ly)] = result;
	}
}
