
#define KERNEL_LENGTH (2 * KERNEL_RADIUS + 1)

#define DEPTH_THRESHOLD	0.025f
#define NORM_THRESHOLD	0.9f

// These functions define discontinuities
bool IsNormalDiscontinuity(float3 n1, float3 n2) {
	return fabs(dot(n1, n2)) < NORM_THRESHOLD;
}

bool IsDepthDiscontinuity(float d1, float d2) {
	return fabs(d1 - d2) > DEPTH_THRESHOLD;
}

bool IsDiscontinuity(float4 t1, float4 t2) {
	return IsDepthDiscontinuity(t1.w, t2.w) || IsNormalDiscontinuity(t1.xyz, t2.xyz);
}

// macros for easier image access
#define INDEX(x, y) ((y) * Pitch + (x))
#define CHECK_BOUNDS(i, s) (((i) >= 0) && ((i) < (s)))
#define H_DATA(img, x, y) (CHECK_BOUNDS(x, Width) ? ((img)[INDEX(x, y)]) : 0)
#define V_DATA(img, x, y) (CHECK_BOUNDS(y, Height) ? ((img)[INDEX(x, y)]) : 0)



//////////////////////////////////////////////////////////////////////////////////////////////////////
// Horizontal convolution filter

//require matching work-group size
__kernel __attribute__((reqd_work_group_size(H_GROUPSIZE_X, H_GROUPSIZE_Y, 1)))
void DiscontinuityHorizontal(
			__global int* d_Disc,
			__global const float4* d_NormDepth,
			int Width,
			int Height,
			int Pitch
			)
{
	__local float4 tile[H_GROUPSIZE_Y][(H_RESULT_STEPS + 2) * H_GROUPSIZE_X];

	int2 lid;
	lid.x = get_local_id(0);
	lid.y = get_local_id(1);

	int2 lb;
	lb.x = get_group_id(0);
	lb.y = get_group_id(1);

	const int baseX = lb.x * H_RESULT_STEPS * H_GROUPSIZE_X - H_GROUPSIZE_X;
	const int baseY = lb.y * H_GROUPSIZE_Y;

	// Load left halo (each thread loads exactly one)
	// Load main data + right halo
#pragma unroll
	for (int tileID = 0; tileID < H_RESULT_STEPS + 2; tileID++)
	{
		tile[lid.y][H_GROUPSIZE_X * tileID + lid.x] = (float4)H_DATA(d_NormDepth, baseX + H_GROUPSIZE_X * tileID + lid.x, baseY + lid.y);
	}

	// Sync threads
	barrier(CLK_LOCAL_MEM_FENCE);

	// Identify discontinuities
#pragma unroll
	for (int tileID = 1; tileID <= H_RESULT_STEPS; tileID++)
	{
		int lx = H_GROUPSIZE_X * tileID + lid.x;
		if (!CHECK_BOUNDS(baseX + lx, Width))
			continue;

		int flag = 0;

		float4 myTile = tile[lid.y][lx];
		float4 leftTile = tile[lid.y][lx - 1];
		float4 rightTile = tile[lid.y][lx + 1];

		// Check the left neighbor
		if (IsDiscontinuity(myTile, leftTile))
		{
			flag |= 1;
		}

		// Check the right neighbor
		if (IsDiscontinuity(myTile, rightTile))
		{
			flag |= 2;
		}

		d_Disc[INDEX(baseX + lx, baseY + lid.y)] = flag;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Vertical convolution filter

//require matching work-group size
__kernel __attribute__((reqd_work_group_size(V_GROUPSIZE_X, V_GROUPSIZE_Y, 1)))
void DiscontinuityVertical(
			__global int* d_Disc,
			__global const float4* d_NormDepth,
			int Width,
			int Height,
			int Pitch
			)
{
	__local float4 tile[(V_RESULT_STEPS + 2) * V_GROUPSIZE_Y][V_GROUPSIZE_X];

	int2 lid;
	lid.x = get_local_id(0);
	lid.y = get_local_id(1);

	int2 lb;
	lb.x = get_group_id(0);
	lb.y = get_group_id(1);

	const int baseX = lb.x * V_GROUPSIZE_X;
	const int baseY = lb.y * V_RESULT_STEPS * V_GROUPSIZE_Y - V_GROUPSIZE_Y;

	// Load left halo (each thread loads exactly one)
	// Load main data + right halo
#pragma unroll
	for (int tileID = 0; tileID < V_RESULT_STEPS + 2; tileID++)
	{
		tile[V_GROUPSIZE_Y * tileID + lid.y][lid.x] = (float4)V_DATA(d_NormDepth, baseX + lid.x, baseY + V_GROUPSIZE_Y * tileID + lid.y);
	}

	// Sync threads
	barrier(CLK_LOCAL_MEM_FENCE);

	// Identify discontinuities
#pragma unroll
	for (int tileID = 1; tileID <= V_RESULT_STEPS; tileID++)
	{
		int ly = V_GROUPSIZE_Y * tileID + lid.y;
		if (!CHECK_BOUNDS(baseY + ly, Height))
			continue;

		int flag = 0;

		float4 myTile = tile[ly][lid.x];
		float4 topTile = tile[ly - 1][lid.x];
		float4 bottomTile = tile[ly + 1][lid.x];

		// Check the left neighbor
		if (IsDiscontinuity(myTile, topTile))
		{
			flag |= 4;
		}

		// Check the right neighbor
		if (IsDiscontinuity(myTile, bottomTile))
		{
			flag |= 8;
		}

		d_Disc[INDEX(baseX + lid.x, baseY + ly)] |= flag;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Horizontal convolution filter

//require matching work-group size
__kernel __attribute__((reqd_work_group_size(H_GROUPSIZE_X, H_GROUPSIZE_Y, 1)))
void ConvHorizontal(
			__global float* d_Dst,
			__global const float* d_Src,
			__global const int* d_Disc,
			__constant float* c_Kernel,
			int Width,
			int Height,
			int Pitch
			)
{
	// This will be very similar to the separable convolution, except that you have
	// also load the discontinuity buffer into the local memory
	// Each work-item loads H_RESULT_STEPS values + 2 halo values
	__local float tile[H_GROUPSIZE_Y][(H_RESULT_STEPS + 2) * H_GROUPSIZE_X];
	__local int   disc[H_GROUPSIZE_Y][(H_RESULT_STEPS + 2) * H_GROUPSIZE_X];

	int2 lid;
	lid.x = get_local_id(0);
	lid.y = get_local_id(1);

	int2 lb;
	lb.x = get_group_id(0);
	lb.y = get_group_id(1);

	const int baseX = lb.x * H_RESULT_STEPS * H_GROUPSIZE_X - H_GROUPSIZE_X;
	const int baseY = lb.y * H_GROUPSIZE_Y;

	// Load data to the tile and disc local arrays
#pragma unroll
	for (int tileID = 0; tileID < H_RESULT_STEPS + 2; tileID++)
	{
		tile[lid.y][H_GROUPSIZE_X * tileID + lid.x] = H_DATA(d_Src, baseX + H_GROUPSIZE_X * tileID + lid.x, baseY + lid.y);
		disc[lid.y][H_GROUPSIZE_X * tileID + lid.x] = H_DATA(d_Disc, baseX + H_GROUPSIZE_X * tileID + lid.x, baseY + lid.y);
	}

	// Sync threads
	barrier(CLK_LOCAL_MEM_FENCE);

	// During the convolution iterate inside-out from the center pixel towards the borders.
#pragma unroll
	for (int tileID = 1; tileID <= H_RESULT_STEPS; tileID++) // Iterate over tiles
	{
		int lx = H_GROUPSIZE_X * tileID + lid.x;
		if (!CHECK_BOUNDS(baseX + lx, Width))
			continue;

		float weights = c_Kernel[KERNEL_RADIUS];
		float result = tile[lid.y][lx] * weights;

		// When you iterate to the left, check for 'left' discontinuities. 
		for (int i = -1; i >= -KERNEL_RADIUS; i--)
		{
			// If you find relevant discontinuity, stop iterating
			if (baseX + lx + i < 0 || disc[lid.y][lx + i + 1] & 1)
				break;

			float weight = c_Kernel[KERNEL_RADIUS - i];
			weights += weight;
			result += tile[lid.y][lx + i] * weight;
		}

		// When iterating to the right, check for 'right' discontinuities.
		for (int i = 1; i <= KERNEL_RADIUS; i++)
		{
			// If you find relevant discontinuity, stop iterating
			if (baseX + lx + i >= Width || disc[lid.y][lx + i - 1] & 2)
				break;

			float weight = c_Kernel[KERNEL_RADIUS - i];
			weights += weight;
			result += tile[lid.y][lx + i] * weight;
		}

		// Don't forget to accumulate the weights to normalize the kernel (divide the pixel value by the summed weights)
		d_Dst[INDEX(baseX + lx, baseY + lid.y)] = result / weights;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Vertical convolution filter

//require matching work-group size
__kernel __attribute__((reqd_work_group_size(V_GROUPSIZE_X, V_GROUPSIZE_Y, 1)))
void ConvVertical(
			__global float* d_Dst,
			__global const float* d_Src,
			__global const int* d_Disc,
			__constant float* c_Kernel,
			int Width,
			int Height,
			int Pitch
			)
{
	__local float tile[(V_RESULT_STEPS + 2) * V_GROUPSIZE_Y][V_GROUPSIZE_X];
	__local int   disc[(V_RESULT_STEPS + 2) * V_GROUPSIZE_Y][V_GROUPSIZE_X];

	int2 lid;
	lid.x = get_local_id(0);
	lid.y = get_local_id(1);

	int2 lb;
	lb.x = get_group_id(0);
	lb.y = get_group_id(1);

	const int baseX = lb.x * V_GROUPSIZE_X;
	const int baseY = lb.y * V_RESULT_STEPS * V_GROUPSIZE_Y - V_GROUPSIZE_Y;

	// Load data to the tile and disc local arrays
#pragma unroll
	for (int tileID = 0; tileID < V_RESULT_STEPS + 2; tileID++)
	{
		tile[V_GROUPSIZE_Y * tileID + lid.y][lid.x] = V_DATA(d_Src, baseX + lid.x, baseY + V_GROUPSIZE_Y * tileID + lid.y);
		disc[V_GROUPSIZE_Y * tileID + lid.y][lid.x] = V_DATA(d_Disc, baseX + lid.x, baseY + V_GROUPSIZE_Y * tileID + lid.y);
	}

	// Sync threads
	barrier(CLK_LOCAL_MEM_FENCE);

	// During the convolution iterate inside-out from the center pixel towards the borders.
#pragma unroll
	for (int tileID = 1; tileID <= V_RESULT_STEPS; tileID++) // Iterate over tiles
	{
		int ly = V_GROUPSIZE_Y * tileID + lid.y;
		if (!CHECK_BOUNDS(baseY + ly, Height))
			continue;

		float weights = c_Kernel[KERNEL_RADIUS];
		float result = tile[ly][lid.x] * weights;

		// When you iterate to the top, check for 'top' discontinuities. 
		for (int i = -1; i >= -KERNEL_RADIUS; i--)
		{
			// If you find relevant discontinuity, stop iterating
			if (baseY + ly + i < 0 || disc[ly + i + 1][lid.x] & 4)
				break;

			float weight = c_Kernel[KERNEL_RADIUS - i];
			weights += weight;
			result += tile[ly + i][lid.x] * weight;
		}

		// When iterating to the bottom, check for 'bottom' discontinuities.
		for (int i = 1; i <= KERNEL_RADIUS; i++)
		{
			// If you find relevant discontinuity, stop iterating
			if (baseY + ly + i >= Height || disc[ly + i - 1][lid.x] & 8)
				break;

			float weight = c_Kernel[KERNEL_RADIUS - i];
			weights += weight;
			result += tile[ly + i][lid.x] * weight;
		}

		// Don't forget to accumulate the weights to normalize the kernel (divide the pixel value by the summed weights)
		d_Dst[INDEX(baseX + lid.x, baseY + ly)] = result / weights;
	}
}
