
__kernel void
set_array_to_constant(
	__global int *array,
	int num_elements,
	int val
)
{
	// There is no need to touch this kernel
	if(get_global_id(0) < num_elements)
		array[get_global_id(0)] = val;
}

// macros for easier image access
#define INDEX(x, y) ((y) * pitch + (x))
#define CHECK_BOUNDS(i, s) ((i) >= 0 && (i) < (s))

__kernel void
compute_histogram(
	__global int *histogram,   // accumulate histogram here
	__global const float *img, // input image
	int width,                 // image width
	int height,                // image height
	int pitch,                 // image pitch
	int num_hist_bins          // number of histogram bins
)
{
	int2 gid;
	gid.x = get_global_id(0);
	gid.y = get_global_id(1);

	if (CHECK_BOUNDS(gid.x, width) && CHECK_BOUNDS(gid.y, height))
	{
		float data = img[INDEX(gid.x, gid.y)] * num_hist_bins;
		atomic_inc(&histogram[clamp((int) data, 0, num_hist_bins - 1)]);
	}
} 

__kernel void
compute_histogram_local_memory(
	__global int *histogram,   // accumulate histogram here
	__global const float *img, // input image
	int width,                 // image width
	int height,                // image height
	int pitch,                 // image pitch
	int num_hist_bins,         // number of histogram bins
	__local int *local_hist
)
{
	// init local_hist with zeros since c++ code does not do that...
	int2 lid;
	lid.x = get_local_id(0);
	lid.y = get_local_id(1);

	int localHistId = lid.y * get_local_size(0) + lid.x;
	if (localHistId < num_hist_bins)
	{
		local_hist[localHistId] = 0;
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	// compute histogram
	int2 gid;
	gid.x = get_global_id(0);
	gid.y = get_global_id(1);

	if (CHECK_BOUNDS(gid.x, width) && CHECK_BOUNDS(gid.y, height))
	{
		float data = img[INDEX(gid.x, gid.y)] * num_hist_bins;
		atomic_inc(local_hist + clamp((int) data, 0, num_hist_bins - 1));
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	// write back histogram
	if (localHistId < num_hist_bins)
	{
		atomic_add(histogram + localHistId, local_hist[localHistId]);
	}
}
