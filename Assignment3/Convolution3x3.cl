/*
We assume a 3x3 (radius: 1) convolution kernel, which is not separable.
Each work-group will process a (TILE_X x TILE_Y) tile of the image.
For coalescing, TILE_X should be multiple of 16.

Instead of examining the image border for each kernel, we recommend to pad the image
to be the multiple of the given tile-size.
*/

//should be multiple of 32 on Fermi and 16 on pre-Fermi...
#define TILE_X 32 

#define TILE_Y 16

// macros for easier image access
#define INDEX(x, y) ((y) * Pitch + (x))
#define CHECK_BOUNDS(i, s) ((i) >= 0 && (i) < (s))
#define DATA(x, y) (CHECK_BOUNDS(x, Width) && CHECK_BOUNDS(y, Height) ? d_Src[INDEX(x, y)] : 0)

// d_Dst is the convolution of d_Src with the kernel c_Kernel
// c_Kernel is assumed to be a float[11] array of the 3x3 convolution constants, one multiplier (for normalization) and an offset (in this order!)
// With & Height are the image dimensions (should be multiple of the tile size)
__kernel __attribute__((reqd_work_group_size(TILE_X, TILE_Y, 1)))
void Convolution(
				__global float* d_Dst,
				__global const float* d_Src,
				__constant float* c_Kernel,
				uint Width,  // Use width to check for image bounds
				uint Height,
				uint Pitch   // Use pitch for offsetting between lines
				)
{
	// OpenCL allows to allocate the local memory from 'inside' the kernel (without using the clSetKernelArg() call)
	// in a similar way to standard C.
	// the size of the local memory necessary for the convolution is the tile size + the halo area
	__local float tile[TILE_Y + 2][TILE_X + 2];

	int2 gid;
	gid.x = get_global_id(0);
	gid.y = get_global_id(1);

	int2 lid;
	lid.x = get_local_id(0);
	lid.y = get_local_id(1);

	int2 lb;
	lb.x = get_group_id(0);
	lb.y = get_group_id(1);

	// Fill the halo with zeros
	// not needed here because we overwrite it later any ways
	// therefore later we check if we are within the image's bounds and if not we write 0

	// Load main filtered area from d_Src
	tile[lid.y + 1][lid.x + 1] = d_Src[INDEX(gid.x, gid.y)];

	// Load halo regions from d_Src (edges and corners separately), check for image bounds!
	if (lid.y == 0)
	{
		// write first row w/o corners
		tile[lid.y][lid.x + 1] = DATA(gid.x, gid.y - 1);
	}

	if (lid.y + 2 == TILE_Y + 1)
	{
		// write last row w/o corners
		tile[lid.y + 2][lid.x + 1] = DATA(gid.x, gid.y + 1);
	}

	if (lid.y == 1 && lid.x + 1 < TILE_Y + 2)
	{
		// write first and last column
		tile[lid.x + 1][0] = DATA(lb.x * TILE_X - 1, lb.y * TILE_Y + lid.x);
		tile[lid.x + 1][TILE_X + 1] = DATA(lb.x * TILE_X + TILE_X, lb.y * TILE_Y + lid.x);
	}

	if (lid.y == 2 && lid.x == 0)
	{
		// write corners
		tile[0][0] = DATA(gid.x - 1, gid.y - 2 - 1);
		tile[0][TILE_X + 1] = DATA(gid.x + TILE_X, gid.y - 2 - 1);
		tile[TILE_Y + 1][0] = DATA(gid.x - 1, gid.y - 2 + TILE_Y);
		tile[TILE_Y + 1][TILE_X + 1] = DATA(gid.x + TILE_X, gid.y - 2 + TILE_Y);
	}

	// Sync threads
	barrier(CLK_LOCAL_MEM_FENCE);

	// Perform the convolution and store the convolved signal to d_Dst.
	if (gid.x < Width && gid.y < Height)
	{
		float result = 0;
#pragma unroll
		for (int i = 0; i < 3; i++)
		{
			int y = lid.y + i;
#pragma unroll
			for (int j = 0; j < 3; j++)
			{
				result += tile[y][lid.x + j] * c_Kernel[i * 3 + j];
			}
		}

		d_Dst[INDEX(gid.x, gid.y)] = result * c_Kernel[9] + c_Kernel[10];
	}
}
